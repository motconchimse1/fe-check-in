FROM circleci/node:14

WORKDIR /usr/src/app/
USER root
COPY package.json ./
RUN yarn

COPY ./ ./

CMD ["npm", "run", "build"]
import { request } from 'umi';
import { getUserToken } from '@/helper/functionHelper';

export async function getAllStudentAttend(body, options) {
  const formatDate = (date) => date.split("/").reverse().join("/");
  let url = `${URL_API}/student-attends?page=${body.current}&per_page=${body.pageSize}`;
  if (body.Student) url += `&name=${body.Student.label}`;
  if (body.status) url += `&status=${body.status.label}`;
  if (body.date) url += `&fromDay=${formatDate(body.date[0])}&toDay=${formatDate(body.date[1])}`;

  return request(url, {
    method: 'GET',
    headers: getUserToken(),
    ...(options || {}),
  });
}

export async function getAllStudents(body, options) {
  let url;
  url = `${URL_API}/students`;

  return request(url, {
    method: 'GET',
    headers: getUserToken(),
    ...(options || {}),
  });
}

export async function getStatusOption(id, options) {
  let url;
  url = `${URL_API}/options/${id}`;

  return request(url, {
    method: 'GET',
    headers: getUserToken(),
    ...(options || {}),
  });
}

export async function getStudentAttend(options) {
  let url = `${URL_API}/student-attends/${options}`;

  return request(url, {
    method: 'GET',
    headers: getUserToken(),
  });
}

export async function checkAllAttendance(body, options) {
  return request(`${URL_API}/student-attends/check`, {
    method: 'POST',
    headers: getUserToken(),
    data: {
      ...body,
    },
    ...(options || {}),
  });
}

export async function createStudentAttend(body, options) {
  return request(`${URL_API}/student-attends`, {
    method: 'POST',
    headers: getUserToken(),
    data: {
      ...body,
    },
    ...(options || {}),
  });
}

export async function updateStudentAttend(body, id) {
  return request(`${URL_API}/student-attends/${id}`, {
    method: 'PATCH',
    headers: getUserToken(),
    data: body,
    ...(id || {}),
  });
}

export async function removeStudentAttend(id, options) {
  return request(`${URL_API}/student-attends/${id}`, {
    method: 'DELETE',
    headers: getUserToken(),
    ...(options || {}),
  });
}

export async function exportFile(body, options) {
  let url;
  url = `${URL_API}/student-attends/downloadExcel`;

  return request(url, {
    method: 'GET',
    headers: getUserToken(),
    ...(options || {}),
  });
}

export async function getAllAtendace(body, options) {
  const { fromDay, toDay } = body;
  let url;
  url = `${URL_API}/student-attends/allAttendance/${fromDay}/${toDay}`;

  return request(url, {
    method: 'GET',
    headers: getUserToken(),
    // ...(options || {}),
  });
}

export async function getAllAtendace2(body, options) {
  const { fromDay, toDay } = body;
  let url;
  url = `${URL_API}/student-attends/allAttendance2/${fromDay}/${toDay}`;

  return request(url, {
    method: 'GET',
    headers: getUserToken(),
    // ...(options || {}),
  });
}

export async function saveAllAttendance(body, options) {
  return request(`${URL_API}/student-attends/save`, {
    method: 'POST',
    headers: getUserToken(),
    data: {
      ...body,
    },
    ...(options || {}),
  });
}

export async function getColDate(body, options) {
  const { a, b } = body;
  let url;
  url = `${URL_API}/student-attends/dates/${a}/${b}`;

  return request(url, {
    method: 'GET',
    headers: getUserToken(),
    // ...(options || {}),
  });
}

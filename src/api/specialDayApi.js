import { request } from 'umi';
import { getUserToken } from '@/helper/functionHelper';

export async function getAllSDay(body, options) {
  const formatDate = (date) => date.split('/').reverse().join('/');
  let url = `${URL_API}/days?page=${body.current}&per_page=${body.pageSize}`;
  if (body.date) url += `&fromDay=${formatDate(body.date[0])}&toDay=${formatDate(body.date[1])}`;

  return request(url, {
    method: 'GET',
    headers: getUserToken(),
    ...(options || {}),
  });
}

export async function getSDay(options) {
  let url = `${URL_API}/days/${options}`;

  return request(url, {
    method: 'GET',
    headers: getUserToken(),
  });
}

export async function createHoliday(options) {
  let url = `${URL_API}/days/holiday/${options}`;

  return request(url, {
    method: 'POST',
    headers: getUserToken(),
  });
}

export async function createSDay(body, options) {
  return request(`${URL_API}/days`, {
    method: 'POST',
    headers: getUserToken(),
    data: {
      ...body,
    },
    ...(options || {}),
  });
}

export async function updateSDay(body, id) {
  return request(`${URL_API}/days/${id}`, {
    method: 'PATCH',
    headers: getUserToken(),
    data: body,
    ...(id || {}),
  });
}

export async function removeSDay(id, options) {
  return request(`${URL_API}/days/${id}`, {
    method: 'DELETE',
    headers: getUserToken(),
    ...(options || {}),
  });
}

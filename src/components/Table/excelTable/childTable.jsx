import React, { useRef } from 'react';

const ChildTable = ({
  optionData,
  name,
  childColumn,
  Child,
  fromDay,
  toDay,
  numDay,
  migraData,
  // loading,
}) => {
  const actionRef = useRef();

  return (
    <Child
      name={name}
      childColumn={childColumn}
      actionRef={actionRef}
      fromDay={fromDay}
      toDay={toDay}
      optionData={optionData}
      numDay={numDay}
      migraData={migraData}
      // loading={loading}
      // getAllData={getAllData}
    />
  );
};

export default ChildTable;

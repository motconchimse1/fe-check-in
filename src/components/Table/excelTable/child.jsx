import React from 'react';
import ProTable from '@ant-design/pro-table';
import moment from 'moment';

const TableList = ({ fromDay, toDay, migraData, childColumn }) => {
  return (
    <>
      <ProTable
        bordered
        scroll={{ x: 1000, y: 500 }}
        size="small"
        headerTitle={
          fromDay !== 'null' &&
          `${moment(new Date(fromDay)).format('DD/MM/YYYY')} -> ${moment(new Date(toDay)).format(
            'DD/MM/YYYY',
          )}`
        }
        rowKey="id"
        // loading={loading}
        search={false}
        dataSource={migraData}
        columns={childColumn}
        pagination={{
          defaultPageSize: 100,
        }}
      />
    </>
  );
};

export default TableList;

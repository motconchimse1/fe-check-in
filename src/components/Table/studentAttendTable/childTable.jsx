import React, { useState, useRef } from 'react';
import { message } from 'antd';
import { useRequest } from 'umi';
import {
  getStatusOption,
  createStudentAttend,
  checkAllAttendance,
  getAllStudents,
} from '@/api/studentAttendApi';
import CreateForm from './components/CreateForm';
import UpdateForm from './components/UpdateForm';
import CreateDateForm from './components/CreateDateForm';
import CreateRangeDateForm from './components/CreateRangeDateForm';
import ViewForm from './components/ViewForm';
import { useEffect } from 'react';

const ChildTable = ({
  optionData,
  childData,
  FormItem,
  FormCreateItem,
  FormCreateItem2,
  name,
  childColumn,
  getAllData,
  getAllData2,
  removeData,
  createData,
  updateData,
  Child,
  fromDay,
  toDay,
  setFromDay,
  setToDay,
  getCol,
  updateModalVisible,
  handleUpdateModalVisible,
  createDateVisible,
  handleCreateDateVisible,
  currentRow,
  setCurrentRow,
  saveData,
  numDay,
  migraData,
  setMigraData,
  // loading,
  // setLoading,
}) => {
  const actionRef = useRef();

  const { data: statusData, run: optionRun } = useRequest(getStatusOption, {
    manual: true,
    formatResult: (res) =>
      res.data.map((el) => {
        return { label: el.name, value: el.val };
      }),
  });
  const { data: studentData, run: studentRun } = useRequest(getAllStudents, {
    manual: true,
    formatResult: (res) =>
      res.data.map((el) => {
        return { label: el.name, value: el.id };
      }),
  });

  useEffect(() => {
    optionRun(1);
    studentRun();
  }, []);

  const [createModalVisible, handleModalVisible] = useState(false);
  const [createModalVisible2, handleModalVisible2] = useState(false);
  const [viewModalVisible, handleViewModalVisible] = useState(false);
  const [onOk, setOnOk] = useState(false);

  const handleRemove = async (selectedRows) => {
    const hide = message.loading('Loading...');
    if (!selectedRows) return true;

    try {
      for (let i of selectedRows) {
        await removeData(i.id);
      }
      hide();
      message.success('Success');
      return true;
    } catch (error) {
      hide();
      message.error('Fail, Please try again!');
      return false;
    }
  };

  const createModal = (
    <CreateForm
      FormItem={FormItem}
      childData={childData}
      name={name}
      createModalVisible={createModalVisible}
      handleModalVisible={handleModalVisible}
      actionRef={actionRef}
      createData={createData}
      fromDay={fromDay}
      toDay={toDay}
      setFromDay={setFromDay}
      setToDay={setToDay}
      getCol={getCol}
      getAllData={getAllData}
      setOnOk={setOnOk}
    />
  );

  const handleDone = () => {
    handleUpdateModalVisible(false);
    setCurrentRow({});
  };

  const handleDone2 = () => {
    handleCreateDateVisible(false);
    setCurrentRow({});
  };

  const createDateModal = (
    <CreateDateForm
      FormItem={FormCreateItem}
      childData={statusData}
      name={name}
      current={currentRow}
      createModalVisible={createDateVisible}
      handleModalVisible={handleCreateDateVisible}
      actionRef={actionRef}
      createData={createStudentAttend}
      onDone={handleDone2}
    />
  );

  const createRangeDateModal = (
    <CreateRangeDateForm
      FormItem={FormCreateItem2}
      childData={statusData}
      childData2={studentData}
      name={name}
      current={currentRow}
      createModalVisible={createModalVisible2}
      handleModalVisible={handleModalVisible2}
      actionRef={actionRef}
      createData={createStudentAttend}
      checkData={checkAllAttendance}
      onDone={handleDone2}
      fromDay={fromDay}
      toDay={toDay}
    />
  );

  const updateModal = (
    <UpdateForm
      FormItem={FormItem}
      childData={statusData}
      setMigraData={setMigraData}
      migraData={migraData}
      name={name}
      visible={updateModalVisible}
      setVisible={handleUpdateModalVisible}
      current={currentRow}
      onDone={handleDone}
      actionRef={actionRef}
      updateData={updateData}
      viewModalVisible={viewModalVisible}
      // setLoading={setLoading}
    />
  );

  const viewModal = (
    <ViewForm
      visible={viewModalVisible}
      setVisible={handleViewModalVisible}
      fromDay={fromDay}
      toDay={toDay}
      columns={childColumn}
      numDay={numDay}
      optionData={optionData}
      migraData={migraData}
      setMigraData={setMigraData}
      onOk={onOk}
      setOnOk={setOnOk}
      statusData={statusData}
      saveData={saveData}
      getAllData={getAllData}
      actionRef={actionRef}
    />
  );

  return (
    <Child
      name={name}
      columns={childColumn}
      updateModalVisible={updateModalVisible}
      handleUpdateModalVisible={handleUpdateModalVisible}
      handleModalVisible2={handleModalVisible2}
      handleModalVisible={handleModalVisible}
      actionRef={actionRef}
      handleRemove={handleRemove}
      createModal={createModal}
      createDateModal={createDateModal}
      createRangeDateModal={createRangeDateModal}
      updateModal={updateModal}
      viewModal={viewModal}
      fromDay={fromDay}
      toDay={toDay}
      optionData={optionData}
      saveData={saveData}
      numDay={numDay}
      migraData={migraData}
      getAllData={getAllData}
      getAllData2={getAllData2}
      visible={viewModalVisible}
      setVisible={handleViewModalVisible}
      onOk={onOk}
      setOnOk={setOnOk}
    />
  );
};

export default ChildTable;

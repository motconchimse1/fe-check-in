import React, { useState } from 'react';
import { ModalForm } from '@ant-design/pro-form';
import { message, Button, Popconfirm } from 'antd';

const CreateDateForm = ({
  FormItem,
  childData,
  actionRef,
  name,
  current,
  createModalVisible,
  handleModalVisible,
  createData,
  onDone,
}) => {
  const handleAdd = async (fields, bool) => {
    const hide = message.loading('Loading...');
    const { status, note, date } = fields;

    try {
      await createData({ status, note, date: [date, date], studentid: current.studentid, bool });
      hide();
      message.success('Success');
      return true;
    } catch (error) {
      hide();
      message.error('Fail, Please try again!');
      return false;
    }
  };

  return (
    <ModalForm
      title={`Create ${name}`}
      width="400px"
      visible={createModalVisible}
      onVisibleChange={handleModalVisible}
      modalProps={{
        onCancel: () => onDone(),
        destroyOnClose: true,
      }}
      onFinish={async (value) => {
        const success = await handleAdd(value, 1);
        if (success) {
          handleModalVisible(false);

          if (actionRef.current) {
            actionRef.current.reload();
          }
        }
      }}
    >
      <FormItem current={current && current} childData={childData} />
    </ModalForm>
  );
};

export default CreateDateForm;

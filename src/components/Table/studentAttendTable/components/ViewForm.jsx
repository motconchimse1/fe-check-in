import React from 'react';
import { ModalForm } from '@ant-design/pro-form';
import { Modal } from 'antd';
import ExcelTable from '@/pages/management/excelTable';

const ViewForm = ({
  visible,
  setVisible,
  fromDay,
  toDay,
  columns,
  numDay,
  optionData,
  migraData,
  setMigraData,
  setOnOk,
  saveData,
  // loading,
  // setLoading,
  actionRef,
}) => {
  if (!visible) {
    return null;
  }

  const handleSave = async (data) => {
    // setLoading(true);
    const newArr = [];
    const changeArr = [];
    data.forEach((el) => {
      Object.keys(el).forEach((key, index) => {
        if (index < numDay && el[key].eventdate !== null) {
          if (el[key].attendance_id === null && el[key].val !== '') {
            newArr.push({
              studentid: el.studentid,
              note: el[key].note,
              status: el[key].val,
              date: el[key].eventdate,
              time: el[key].eventtime,
              imgurl: el[key].imgurl,
            });
          } else if (el[key].isChange || el[key].isupdated) {
            changeArr.push({
              id: el[key].attendance_id,
              status: el[key].val,
            });
          }
        }
      });
    });
    if (newArr.length || changeArr.length) {
      const res = await saveData({ data: newArr, data2: changeArr });
      if (res.status === 'success') {
        // getAllData(fromDay, toDay);
        if (actionRef.current) {
          actionRef.current.reload();
        }
        Modal.success({
          content: 'Đã lưu thành công',
        });
      }
    }

    // setLoading(false);
  };

  return (
    <ModalForm
      width="100%"
      visible={visible}
      modalProps={{
        onCancel: () => setVisible(false),
        destroyOnClose: true,
      }}
      onFinish={() => {
        handleSave(migraData);
        setOnOk(true);
        setVisible(false);
      }}
    >
      <ExcelTable
        fromDay={fromDay}
        toDay={toDay}
        columns={columns}
        numDay={numDay}
        optionData={optionData}
        migraData={migraData}
        setMigraData={setMigraData}
        // loading={loading}
        // setLoading={setLoading}
      />
    </ModalForm>
  );
};

export default ViewForm;

import React from 'react';
import { ModalForm, ProFormSelect, ProFormTextArea } from '@ant-design/pro-form';
import { Card, Image } from 'antd';
import moment from 'moment';

const { Meta } = Card;

const UpdateForm = ({
  childData,
  visible,
  setVisible,
  current,
  onDone,
  setMigraData,
  migraData,
  updateData,
  viewModalVisible,
  actionRef,
  // setLoading,
}) => {
  let thisDay;
  if (current && current.item) {
    thisDay = new Date(current.item.eventdate);
  }

  if (!visible) {
    return null;
  }

  const fetchUpdate = async (status, note, id) => {
    const res = await updateData({ status, note }, id);
  };

  return (
    <ModalForm
      title={`${moment(thisDay).format('DD/MM/YYYY')} ${
        current.item.eventtime !== null ? current.item.eventtime : ''
      }`}
      width="400px"
      visible={visible}
      modalProps={{
        onCancel: () => onDone(),
        destroyOnClose: true,
      }}
      initialValues={{
        status: {
          label: childData.find((i) => i.value === current.item.val).label,
          value: current.item.val,
        },
      }}
      onFinish={async (value) => {
        setVisible(false);

        if (!viewModalVisible) {
          await fetchUpdate(
            typeof value.status === 'object' ? value.status.value : value.status,
            value.note,
            current.item.attendance_id,
          );
          if (actionRef.current) {
            actionRef.current.reload();
          }
        }

        let newArr = [...migraData];
        newArr[current.index] = {
          ...newArr[current.index],
          [current.day]: {
            ...newArr[current.index][current.day],
            val: typeof value.status === 'object' ? value.status.value : value.status,
            isChange: true,
          },
        };
        setMigraData(newArr);
      }}
    >
      <Meta title={`${current.studentname} | ${current.orgunit}`} />
      <br />

      <ProFormSelect
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        options={childData}
        width="md"
        label=" Status "
        name="status"
      />
      <ProFormTextArea width="md" label=" Ghi chú " name="note" initialValue={current.item.note} />
      <Card
        cover={
          <Image
            // width={200}
            src={current.item.imgurl}
            placeholder={
              <Image
                preview={false}
                src={current.item.imgurl}
                // width={200}
              />
            }
          />
        }
      />
    </ModalForm>
  );
};

export default UpdateForm;

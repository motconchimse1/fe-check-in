import React from 'react';
import { ModalForm } from '@ant-design/pro-form';
import { message } from 'antd';

const CreateForm = ({
  FormItem,
  childData,
  actionRef,
  name,
  createModalVisible,
  handleModalVisible,
  setFromDay,
  setToDay,
  getCol,
  getAllData,
  setOnOk,
}) => {
  const handleAdd = async (fields) => {
    setOnOk(false);
    const hide = message.loading('Loading...');
    const { date } = fields;

    try {
      getCol(date[0], date[1]);
      // getAllData(date[0], date[1]);
      setFromDay(date[0]);
      setToDay(date[1]);
      hide();
      message.success('Success');
      return true;
    } catch (error) {
      hide();
      message.error('Fail, Please try again!');
      return false;
    }
  };

  return (
    <ModalForm
      title={`Create ${name}`}
      width="400px"
      visible={createModalVisible}
      onVisibleChange={handleModalVisible}
      onFinish={async (value) => {
        const success = await handleAdd(value);
        if (success) {
          handleModalVisible(false);

          if (actionRef.current) {
            actionRef.current.reload();
          }
        }
      }}
    >
      <FormItem childData={childData} />
    </ModalForm>
  );
};

export default CreateForm;

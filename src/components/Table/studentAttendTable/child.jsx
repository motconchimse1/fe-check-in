import { CalendarOutlined } from '@ant-design/icons';
import { Button, Modal } from 'antd';
import React, { useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import moment from 'moment';

const TableList = ({
  optionData,
  columns,
  handleModalVisible,
  handleModalVisible2,
  actionRef,
  createModal,
  createRangeDateModal,
  createDateModal,
  updateModal,
  viewModal,
  fromDay,
  toDay,
  getAllData,
  getAllData2,
  setVisible,
}) => {
  const optionColumns = [
    {
      title: 'Name',
      dataIndex: 'name',
      width: 150,
    },
    {
      title: 'Value',
      render: (_, row) => (
        <div
          style={{
            backgroundColor: `#${row.config.bg}`,
            width: '50px',
            border: `1px  solid #000000`,
            textAlign: 'center',
            fontWeight: 'bold',
          }}
        >
          {row.val}
        </div>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable
        bordered
        scroll={{ x: 1000, y: 500 }}
        size="small"
        headerTitle={
          fromDay !== 'null' &&
          `${moment(new Date(fromDay)).format('DD/MM/YYYY')} -> ${moment(new Date(toDay)).format(
            'DD/MM/YYYY',
          )}`
        }
        actionRef={actionRef}
        // loading={loading}
        rowKey="id"
        search={false}
        toolBarRender={() => [
          <Button
            type="primary"
            disabled={fromDay === 'null'}
            key="1"
            onClick={() => {
              handleModalVisible2(true);
            }}
          >
            Add student attendance
          </Button>,
          <Button
            type="primary"
            key="2"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CalendarOutlined /> Select date
          </Button>,
          <Button
            type="primary"
            disabled={fromDay === 'null'}
            key="3"
            onClick={() => {
              getAllData(fromDay, toDay);
              setVisible(true);
            }}
          >
            Import Data
          </Button>,

          <Button
            type="primary"
            key="4"
            disabled={fromDay === 'null'}
            onClick={() =>
              window.open(
                `${URL_API}/student-attends/downloadExcel?fromDay=${fromDay}&toDay=${toDay}`,
              )
            }
          >
            Export File
          </Button>,
        ]}
        request={getAllData2}
        columns={columns}
        pagination={{
          defaultPageSize: 100,
        }}
      />
      {fromDay !== 'null' && (
        <>
          <ProTable
            style={{
              marginBottom: 24,
            }}
            size="small"
            pagination={false}
            search={false}
            options={false}
            toolBarRender={false}
            dataSource={optionData}
            columns={optionColumns}
            rowKey="id"
          />
        </>
      )}

      {createModal}
      {createRangeDateModal}
      {createDateModal}
      {updateModal}
      {viewModal}
    </PageContainer>
  );
};

export default TableList;

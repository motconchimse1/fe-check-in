import { PlusOutlined } from '@ant-design/icons';
import { Button, Dropdown, Menu } from 'antd';
import React, { useState } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';

const TableList = ({
  name,
  columns,
  getAllData,
  handleModalVisible,
  actionRef,
  handleRemove,
  createModal,
  updateModal,
  extendButton,
  getHoliday,
}) => {
  const currentYear = new Date().getFullYear();
  const [selectedRowsState, setSelectedRows] = useState([]);

  const menu = (
    <Menu
      onClick={async (e) => {
        await getHoliday(e.key);
        actionRef.current?.reloadAndRest?.();
      }}
    >
      <Menu.Item key={currentYear}>{currentYear}</Menu.Item>
      <Menu.Item key={currentYear + 1}>{currentYear + 1}</Menu.Item>
    </Menu>
  );

  return (
    <PageContainer>
      <ProTable
        headerTitle={name}
        actionRef={actionRef}
        rowKey="id"
        search={{
          labelWidth: 120,
        }}
        toolBarRender={() => [
          <Button
            type="primary"
            key="first"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> Add {name}
          </Button>,
          extendButton && (
            <Dropdown
              key="second"
              overlay={menu}
              placement="bottom"
              arrow={{ pointAtCenter: true }}
            >
              <Button type="primary">Add Holiday</Button>
            </Dropdown>
          ),
        ]}
        request={getAllData}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
          getCheckboxProps: (record) => ({
            disabled: record.name === 'admin',
            // Column configuration not to be checked
            name: record.name,
          }),
        }}
      />
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              Chosen{' '}
              <a
                style={{
                  fontWeight: 600,
                }}
              >
                {selectedRowsState.length}
              </a>{' '}
              item &nbsp;&nbsp;
              {/* <span>
                Total number of service calls{' '}
                {selectedRowsState.reduce((pre, item) => pre + item.name, 0)} 万
              </span> */}
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            Delete
          </Button>
          <Button type="primary">???</Button>
        </FooterToolbar>
      )}

      {createModal}
      {updateModal}
    </PageContainer>
  );
};

export default TableList;

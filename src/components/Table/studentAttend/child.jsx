import { PlusOutlined } from '@ant-design/icons';
import { Button, Dropdown, Menu } from 'antd';
import React, { useState } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';

const TableList = ({
  name,
  columns,
  getAllData,
  handleModalVisible,
  actionRef,
  handleRemove,
  createModal,
  updateModal,
}) => {
  const [selectedRowsState, setSelectedRows] = useState([]);

  return (
    <PageContainer>
      <ProTable
        headerTitle={name}
        actionRef={actionRef}
        rowKey="id"
        search={{
          labelWidth: 120,
        }}
        toolBarRender={() => [
          <Button
            type="primary"
            key="first"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> Add {name}
          </Button>,
        ]}
        request={getAllData}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
          getCheckboxProps: (record) => ({
            disabled: record.name === 'admin',
            // Column configuration not to be checked
            name: record.name,
          }),
        }}
        dateFormatter="string"
      />
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              Chosen{' '}
              <a
                style={{
                  fontWeight: 600,
                }}
              >
                {selectedRowsState.length}
              </a>{' '}
              item &nbsp;&nbsp;
              {/* <span>
                Total number of service calls{' '}
                {selectedRowsState.reduce((pre, item) => pre + item.name, 0)} 万
              </span> */}
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            Delete
          </Button>
          <Button type="primary">???</Button>
        </FooterToolbar>
      )}

      {createModal}
      {updateModal}
    </PageContainer>
  );
};

export default TableList;

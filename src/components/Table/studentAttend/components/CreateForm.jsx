import React, { useState } from 'react';
import { ModalForm } from '@ant-design/pro-form';
import { message, Button, Popconfirm } from 'antd';

const CreateForm = ({
  FormItem,
  childData,
  childData2,
  actionRef,
  name,
  createModalVisible,
  handleModalVisible,
  createData,
  checkData,
}) => {
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [tempData, setTempData] = useState(null);

  const handleAdd = async (fields, bool) => {
    const hide = message.loading('Loading...');
    const { status, note, date, created_by, Student } = fields;

    try {
      await createData({ status, note, date, created_by, studentid: Student, bool });
      hide();
      message.success('Success');
      return true;
    } catch (error) {
      hide();
      message.error('Fail, Please try again!');
      return false;
    }
  };

  const handleCheck = async (fields) => {
    const { date, Student } = fields;

    try {
      const res = await checkData({ date, studentid: Student });
      if (res.bool) {
        await handleAdd(fields, 1);
        return true;
      } else {
        setTempData(fields);
        setVisible(true);
      }
    } catch (error) {
      message.error('Fail, Please try again!');
    }
  };

  const handleOk = () => {
    setConfirmLoading(true);
    setTimeout(() => {
      handleAdd(tempData, 2);
      setVisible(false);
      setConfirmLoading(false);
      handleModalVisible(false);
      if (actionRef.current) {
        actionRef.current.reload();
      }
    }, 1000);
  };

  return (
    <ModalForm
      title={`Create ${name}`}
      width="400px"
      visible={createModalVisible}
      onVisibleChange={handleModalVisible}
      submitter={{
        submitButtonProps: {
          style: {
            display: 'none',
          },
        },
        render: (props, defaultDoms) => {
          return [
            ...defaultDoms,
            <Popconfirm
              key="ok"
              title="Dữ liệu đã tồn tại, bạn có muốn ghi đè lên không"
              visible={visible}
              onConfirm={handleOk}
              okButtonProps={{ loading: confirmLoading }}
              onCancel={() => setVisible(false)}
            >
              <Button
                type="primary"
                onClick={() => {
                  props.submit();
                }}
              >
                Ok
              </Button>
            </Popconfirm>,
          ];
        },
      }}
      onFinish={async (value) => {
        const success = await handleCheck(value);
        if (success) {
          handleModalVisible(false);

          if (actionRef.current) {
            actionRef.current.reload();
          }
        }
      }}
    >
      <FormItem childData={childData} childData2={childData2} />
    </ModalForm>
  );
};

export default CreateForm;

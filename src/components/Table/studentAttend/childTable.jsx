import React, { useState, useRef } from 'react';
import { message, Popconfirm } from 'antd';
import CreateForm from './components/CreateForm';
import UpdateForm from './components/UpdateForm';

const ChildTable = ({
  childData,
  childData2,
  FormItem,
  FormItem2,
  name,
  childColumn,
  getAllData,
  getHoliday,
  removeData,
  checkData,
  createData,
  updateData,
  Child,
  extendButton,
  exportFile,
}) => {
  const actionRef = useRef();
  const [updateModalVisible, handleUpdateModalVisible] = useState(false);
  const [createModalVisible, handleModalVisible] = useState(false);
  const [currentRow, setCurrentRow] = useState(undefined);

  const handleRemove = async (selectedRows) => {
    const hide = message.loading('Loading...');
    if (!selectedRows) return true;

    try {
      for (let i of selectedRows) {
        await removeData(i.id);
      }
      hide();
      message.success('Success');
      return true;
    } catch (error) {
      hide();
      message.error('Fail, Please try again!');
      return false;
    }
  };

  const actionColumn = [
    {
      title: 'Action',
      dataIndex: 'option',
      valueType: 'option',
      render: (_, record) =>
        record.name !== 'admin' && [
          <Popconfirm
            key={record.name}
            title={`Are you sure to delete this ${name}?`}
            onConfirm={async () => {
              const success = await handleRemove([record]);
              if (success) {
                if (actionRef.current) {
                  actionRef.current.reload();
                }
              }
            }}
            okText="Yes"
            cancelText="No"
          >
            <a key="subscribeAlert" href="#">
              Delete
            </a>
          </Popconfirm>,
          !record.status.isDisable && (
            <a
              key="config"
              onClick={() => {
                handleUpdateModalVisible(true);
                setCurrentRow({
                  ...record,
                });
              }}
            >
              Update
            </a>
          ),
        ],
    },
  ];

  const createModal = (
    <CreateForm
      FormItem={FormItem}
      childData={childData}
      childData2={childData2}
      name={name}
      createModalVisible={createModalVisible}
      handleModalVisible={handleModalVisible}
      actionRef={actionRef}
      createData={createData}
      checkData={checkData}
    />
  );

  const handleDone = () => {
    handleUpdateModalVisible(false);
    setCurrentRow({});
  };

  const updateModal = (
    <UpdateForm
      FormItem={FormItem2}
      childData={childData}
      childData2={childData2}
      name={name}
      visible={updateModalVisible}
      setVisible={handleUpdateModalVisible}
      current={currentRow}
      onDone={handleDone}
      actionRef={actionRef}
      updateData={updateData}
    />
  );

  return (
    <Child
      name={name}
      columns={childColumn.concat(actionColumn)}
      getAllData={getAllData}
      updateModalVisible={updateModalVisible}
      handleUpdateModalVisible={handleUpdateModalVisible}
      createModalVisible={createModalVisible}
      handleModalVisible={handleModalVisible}
      actionRef={actionRef}
      handleRemove={handleRemove}
      createModal={createModal}
      updateModal={updateModal}
      getHoliday={getHoliday}
      extendButton={extendButton}
      exportFile={exportFile}
    />
  );
};

export default ChildTable;

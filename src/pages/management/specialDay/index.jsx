import React, { useRef } from 'react';
import Child from '@/components/Table/specialDay/child';
import ChildTable from '@/components/Table/specialDay/childTable';
import FormItem from './FormItem';
import FormItem2 from './FormItem2';
import moment from 'moment';
import { getAllSDay, createSDay, updateSDay, removeSDay, createHoliday } from '@/api/specialDayApi';
import { useRequest } from 'umi';

const TableList = () => {
  const name = 'Special Day';
  const { run } = useRequest(createHoliday, {
    manual: true,
  });

  const childColumn = [
    {
      title: 'Ngày',
      dataIndex: 'date',
      valueType: 'dateRange',
      fieldProps: (form, config) => ({
        format: 'DD/MM/YYYY',
        ranges: {
          Today: [moment(), moment()],
          'This Year': [moment().startOf('year'), moment().endOf('year')],
          'Last Year': [
            moment().subtract(1, 'years').startOf('year'),
            moment().subtract(1, 'years').endOf('year'),
          ],
          'Next Year': [
            moment().subtract(-1, 'years').startOf('year'),
            moment().subtract(-1, 'years').endOf('year'),
          ],
        },
      }),
      render: (date, record) => moment(new Date(record.date)).format('DD/MM/YYYY'),
    },
    {
      title: 'Tên',
      dataIndex: 'name',
      search: false,
    },
    {
      title: 'Ghi chú',
      dataIndex: 'note',
      search: false,
    },
  ];

  return (
    <ChildTable
      FormItem={FormItem}
      FormItem2={FormItem2}
      name={name}
      childColumn={childColumn}
      getAllData={getAllSDay}
      getHoliday={(e) => run(e)}
      createData={createSDay}
      updateData={updateSDay}
      removeData={removeSDay}
      Child={Child}
      extendButton={true}
    />
  );
};

export default TableList;

import React, { useState } from 'react';
import { ProFormText, ProFormDateRangePicker, ProFormSelect } from '@ant-design/pro-form';
import moment from 'moment';

const FormCreateItem2 = ({ fromDay, toDay, childData, childData2 }) => {
  console.log(fromDay, toDay);
  const [dates, setDates] = useState([]);
  const [hackValue, setHackValue] = useState();
  const [value, setValue] = useState();

  const disabledDate = (current) => {
    return current > moment(toDay).add(1, 'days') || current < moment(fromDay);
  };

  const onOpenChange = (open) => {
    if (open) {
      setHackValue([]);
      setDates([]);
    } else {
      setHackValue(undefined);
    }
  };

  return (
    <>
      <ProFormDateRangePicker
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        fieldProps={{
          value: hackValue || value,
          disabledDate: disabledDate,
          onCalendarChange: (e) => setDates(e),
          onCalendarChange: (val) => setDates(val),
          onChange: (val) => setValue(val),
          onOpenChange: onOpenChange,
          ranges: {
            Today: [moment(), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
          },
        }}
        format="DD/MM/YYYY"
        width="md"
        label=" Chọn ngày "
        name="date"
      />
      <ProFormSelect
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        options={childData2}
        width="md"
        label=" Người nghỉ "
        name="Student"
        showSearch
      />
      <ProFormSelect
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        options={childData.filter((el) => !el.disable)}
        width="md"
        label=" Status "
        name="status"
      />
      <ProFormText width="md" label=" Ghi chú " name="note" />
    </>
  );
};

export default FormCreateItem2;

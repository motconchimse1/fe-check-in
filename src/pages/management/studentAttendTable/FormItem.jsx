import React, { useState } from 'react';
import { ProFormDateRangePicker } from '@ant-design/pro-form';
import moment from 'moment';

const FormItem = () => {
  const [dates, setDates] = useState([]);
  const [hackValue, setHackValue] = useState();
  const [value, setValue] = useState();

  const disabledDate = (current) => {
    if (!dates || dates.length === 0) {
      return false;
    }
    const firstDay = dates[0] && new Date(dates[0]);
    const month = dates[0] && firstDay.getMonth();
    const lastDay = dates[0] && new Date(new Date().getFullYear(), month + 1, 0);
    const difference = dates[0] && lastDay.getTime() - firstDay.getTime();
    const days = dates[0] && Math.ceil(difference / (1000 * 3600 * 24));
    const tooLate = dates[0] && current.diff(dates[0], 'days') > days;
    const tooEarly = dates[1] && dates[1].diff(current, 'days') > days;
    return tooEarly || tooLate;
  };

  const onOpenChange = (open) => {
    if (open) {
      setHackValue([]);
      setDates([]);
    } else {
      setHackValue(undefined);
    }
  };

  return (
    <>
      <ProFormDateRangePicker
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        fieldProps={{
          value: hackValue || value,
          disabledDate: disabledDate,
          onCalendarChange: (e) => setDates(e),
          onCalendarChange: (val) => setDates(val),
          onChange: (val) => setValue(val),
          onOpenChange: onOpenChange,
          ranges: {
            Today: [moment(), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [
              moment().subtract(1, 'months').startOf('month'),
              moment().subtract(1, 'months').endOf('month'),
            ],
          },
        }}
        format="DD/MM/YYYY"
        width="md"
        label=" Chọn ngày "
        name="date"
      />
    </>
  );
};

export default FormItem;

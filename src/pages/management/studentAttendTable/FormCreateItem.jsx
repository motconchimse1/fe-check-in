import React from 'react';
import { ProFormText, ProFormDatePicker, ProFormSelect } from '@ant-design/pro-form';

const FormCreateItem = ({ current, childData }) => {
  return (
    <>
      <ProFormDatePicker
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        format="DD/MM/YYYY"
        width="md"
        label=" Chọn ngày "
        name="date"
        disabled
        initialValue={current.day}
      />
      <ProFormText
        width="md"
        label=" Họ và  tên "
        name="name"
        initialValue={current.studentname}
        disabled
      />
      <ProFormSelect
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        options={childData.filter((el) => !el.disable)}
        width="md"
        label=" Status "
        name="status"
      />
      <ProFormText width="md" label=" Ghi chú " name="note" />
    </>
  );
};

export default FormCreateItem;

import React, { useState } from 'react';
import Child from '@/components/Table/studentAttendTable/child';
import ChildTable from '@/components/Table/studentAttendTable/childTable';
import FormItem from './FormItem';
import FormCreateItem from './FormCreateItem';
import FormCreateItem2 from './FormCreateItem2';
import {
  getAllAtendace,
  getAllAtendace2,
  saveAllAttendance,
  updateStudentAttend,
} from '@/api/studentAttendApi';
import { useRequest } from 'umi';
import { Input, Button, Space } from 'antd';
import Highlighter from 'react-highlight-words';
import { SearchOutlined } from '@ant-design/icons';

const SubChild = ({
  data,
  fromDay,
  toDay,
  setFromDay,
  setToDay,
  getCol,
  optionData,
}) => {
  const {
    data: attendData,
    run: attendRun,
    loading,
  } = useRequest(getAllAtendace2, {
    manual: true,
    formatResult: (res) => res,
  });
  const [migraData, setMigraData] = useState([]);
  const [updateModalVisible, handleUpdateModalVisible] = useState(false);
  const [createDateVisible, handleCreateDateVisible] = useState(false);
  const [currentRow, setCurrentRow] = useState(undefined);
  const [searchText, setSearchText] = useState('');
  const [searchedColumn, setSearchedColumn] = useState('');

  const name = 'Student Attendance';

  const checkColor = optionData.reduce((a, v) => ({ ...a, [v.val]: v.config.bg }), {});

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            // searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText(selectedKeys[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    // onFilterDropdownVisibleChange: (visible) => {
    //   if (visible) {
    //     setTimeout(() => searchInput.select(), 100);
    //   }
    // },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText('');
  };

  const childColumn = [
    {
      title: 'Họ và tên',
      dataIndex: 'studentname',
      key: 'studentname',
      width: 200,
      fixed: 'left',
      ...getColumnSearchProps('studentname'),
    },
    {
      title: 'Thuộc',
      dataIndex: 'orgunit',
      width: 100,
      fixed: 'left',
    },
  ];

  data.forEach((el, bigIndex) => {
    childColumn.push({
      title: el.d,
      dataIndex: el.d,
      width: 40,
      align: 'center',
      render: (item, record, index) => (
        <>
        {/* {console.log(item)} */}
          <div
            key="config"
            onClick={() => {
              if (item === '-') {
                handleCreateDateVisible(true);
                setCurrentRow({
                  studentname: record.studentname,
                  studentid: record.studentid,
                  orgunit: record.orgunit,
                  day: `${new Date(fromDay).getFullYear()}-${new Date(fromDay).getMonth() + 1}-${
                    el.d
                  }`,
                });
              } else {
                const checkArr = ['T7', 'CN', 'NL'];
                if (!checkArr.includes(item.val)) {
                  handleUpdateModalVisible(true);
                  setCurrentRow({
                    studentname: record.studentname,
                    orgunit: record.orgunit,
                    day: bigIndex + 1,
                    index,
                    item,
                  });
                }
              }
            }}
            style={{
              backgroundColor: item !== "-" ? `#${checkColor[item.val]}` : "#fff",
              textAlign: 'center',
              cursor: 'pointer',
            }}
          >
            {item.val || '\u2003'}
          </div>
        </>
      ),
    });
  });

  const fetchData = async (fromDay, toDay) => {
    const res = await getAllAtendace({ fromDay, toDay });
    if (res.status === 'success') {
      setMigraData(res.data);
    }
  };

  return (
    <ChildTable
      FormItem={FormItem}
      FormCreateItem={FormCreateItem}
      FormCreateItem2={FormCreateItem2}
      name={name}
      childColumn={childColumn}
      numDay={data.length}
      getAllData={(a, b) => fetchData(a, b)}
      getAllData2={() => attendRun({ fromDay, toDay })}
      updateData={updateStudentAttend}
      migraData={migraData}
      setMigraData={setMigraData}
      saveData={saveAllAttendance}
      Child={Child}
      fromDay={fromDay}
      toDay={toDay}
      setFromDay={setFromDay}
      setToDay={setToDay}
      getCol={getCol}
      optionData={optionData}
      updateModalVisible={updateModalVisible}
      handleUpdateModalVisible={handleUpdateModalVisible}
      createDateVisible={createDateVisible}
      handleCreateDateVisible={handleCreateDateVisible}
      currentRow={currentRow}
      setCurrentRow={setCurrentRow}
    />
  );
};

export default SubChild;

import React, { useEffect, useState } from 'react';
import SubChild from './subChild';
import { getColDate } from '@/api/studentAttendApi';
import { useRequest } from 'umi';

const TableList = () => {
  const { data, run } = useRequest(getColDate, {
    manual: true,
    formatResult: (res) => res,
  });

  const [fromDay, setFromDay] = useState('null');
  const [toDay, setToDay] = useState('null');

  useEffect(() => {
    run({ fromDay, toDay });
  }, []);


  return (
    data !== undefined && (
      <SubChild
        data={data.data}
        fromDay={fromDay}
        toDay={toDay}
        setFromDay={setFromDay}
        setToDay={setToDay}
        getCol={(a, b) => run({ a, b })}
        optionData={data.options}
      />
    )
  );
};

export default TableList;

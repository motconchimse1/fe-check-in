import React from 'react';
import { ProFormText, ProFormDateRangePicker, ProFormSelect } from '@ant-design/pro-form';

const FormItem = ({ childData, childData2 }) => {
  return (
    <>
      <ProFormDateRangePicker
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        format="DD/MM/YYYY"
        width="md"
        label=" Chọn ngày "
        name="date"
      />
      <ProFormSelect
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        options={childData}
        width="md"
        label=" Người nghỉ "
        name="Student"
        showSearch
      />
      <ProFormSelect
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        options={childData2.filter((el) => !el.disable)}
        width="md"
        label=" Status "
        name="status"
      />
      <ProFormText width="md" label=" Ghi chú " name="note" />
    </>
  );
};

export default FormItem;

import React, { useEffect, useState } from 'react';
import Child from '@/components/Table/studentAttend/child';
import ChildTable from '@/components/Table/studentAttend/childTable';
import FormItem from './FormItem';
import FormItem2 from './FormItem2';
import moment from 'moment';
import {
  getAllStudentAttend,
  getAllStudents,
  createStudentAttend,
  checkAllAttendance,
  updateStudentAttend,
  removeStudentAttend,
  getStatusOption,
  exportFile,
} from '@/api/studentAttendApi';
import { convertArrayToObject } from '@/helper/functionHelper';
import { useRequest } from 'umi';

const TableList = () => {
  const name = 'Student Attendance';

  const [studentData, setStudentData] = useState(null);
  const { data: optionData, run: optionRun } = useRequest(getStatusOption, {
    manual: true,
    formatResult: (res) =>
      res.data.map((el) => {
        return { label: `${el.val} - ${el.name} `, value: el.val, disable: el.isdisable };
      }),
  });
  const { data: studentAttendData, run: studentAttendRun } = useRequest(getAllStudentAttend, {
    manual: true,
    formatResult: (res) => {
      return {
        ...res,
        data: res.data.map((el) => {
          const check = optionData.find((i) => i.value === el.status);
          return {
            ...el,
            status: {
              label: check.label,
              value: el.status,
              isDisable: check.disable,
            },
            Student: { label: el.Student.name, value: el.Student.id, code: el.Student.code },
          };
        }),
      };
    },
  });

  useEffect(() => {
    optionRun(1);
    fetchStudentData();
  }, []);

  // useEffect(() => {
  //   if (optionData) console.log(convertArrayToObject(optionData, 'label'));
  // }, [optionData]);

  const fetchStudentData = async () => {
    const res = await getAllStudents();
    setStudentData(
      res.data.map((el) => {
        return {
          label: el.name,
          value: el.id,
        };
      }),
    );
  };

  const childColumn = [
    {
      title: 'Họ và tên',
      dataIndex: ['Student', 'label'],
      valueType: 'select',
      valueEnum: studentData && convertArrayToObject(studentData, 'label'),
      fieldProps: (form, config) => ({
        showSearch: true,
        filterOption: (input, option) =>
          option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0,
      }),
    },
    {
      title: 'Status',
      dataIndex: ['status', 'label'],
      valueType: 'select',
      valueEnum: optionData && convertArrayToObject(optionData, 'value'),
      fieldProps: (form, config) => ({
        showSearch: true,
        filterOption: (input, option) =>
          option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0,
      }),
    },
    {
      title: 'Ghi chú',
      dataIndex: 'note',
      search: false,
    },
    {
      title: 'Người tạo',
      dataIndex: 'createdby',
      search: false,
    },
    {
      title: 'Date',
      dataIndex: 'date',
      valueType: 'dateRange',
      fieldProps: (form, config) => ({
        format: 'DD/MM/YYYY',
        ranges: {
          Today: [moment(), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [
            moment().subtract(1, 'months').startOf('month'),
            moment().subtract(1, 'months').endOf('month'),
          ],
        },
      }),
      render: (date, record) => moment(new Date(record.date)).format('DD/MM/YYYY'),
    },
  ];

  if (optionData && optionData.length) {
    return (
      <ChildTable
        FormItem={FormItem}
        FormItem2={FormItem2}
        name={name}
        childColumn={childColumn}
        childData={studentData}
        childData2={optionData}
        getAllData={studentAttendRun}
        createData={createStudentAttend}
        checkData={checkAllAttendance}
        updateData={updateStudentAttend}
        removeData={removeStudentAttend}
        Child={Child}
        extendButton={true}
        exportFile={exportFile}
      />
    );
  }
  return '';
};

export default TableList;

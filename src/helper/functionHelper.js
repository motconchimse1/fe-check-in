import Cookies from 'js-cookie';
import jwt from 'jwt-decode';

const getUserId = () => {
  const token = Cookies.get('usertoken');
  if (token) {
    const user = jwt(token);
    if (user) return user.id;
    return -1;
  }
};

const getUserToken = (bool) => {
  if (bool)
    return {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${Cookies.get('usertoken')}`,
    };
  return {
    'Content-Type': 'application/json',
  };
};

const convertArrayToObject = (array, key) => {
  const initialValue = {};
  return array.reduce((obj, item) => {
    return {
      ...obj,
      [item[key]]: {text: item.label},
    };
  }, initialValue);
};

const convertArrayToObject2 = (array, key) => {
  const initialValue = {};
  return array.reduce((obj, item) => {
    return {
      ...obj,
      [item[key]]: {text: `${item.value} - ${item.label}`},
    };
  }, initialValue);
};

export { getUserId, getUserToken, convertArrayToObject, convertArrayToObject2 };
